from django.shortcuts import render, redirect
from receipts.models import (
    Receipt,
    ReceiptForm,
    ExpenseCategory,
    Account,
    ExpenseForm,
    AccountForm,
)
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(redirect_field_name="user_login")
def show_receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)


@login_required(redirect_field_name="user_login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            # receipt.date = form.
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/categories.html", context)


# this part of the render needs work


def accounts_list(request):
    accounts_list = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts_list,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            accounts = form.save(False)
            accounts.owner = request.user
            accounts.save()
            return redirect("accounts_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            categories = form.save(False)
            categories.owner = request.user
            categories.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)
