from django.urls import path
from receipts.views import (
    show_receipt_list,
    create_receipt,
    category_list,
    accounts_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("receipts/", show_receipt_list, name="home"),
    path("receipts/create/", create_receipt, name="create_receipt"),
    path("receipts/categories/", category_list, name="category_list"),
    path(
        "receipts/categories/create/", create_category, name="create_category"
    ),
    path("receipts/accounts/", accounts_list, name="accounts_list"),
    path("receipts/accounts/create/", create_account, name="create_account"),
]
